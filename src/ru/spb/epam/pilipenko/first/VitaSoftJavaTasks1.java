package ru.spb.epam.pilipenko.first;

import com.sun.xml.internal.fastinfoset.util.CharArray;

import java.util.*;

public class VitaSoftJavaTasks1 implements ISolver{

    public static void main(String[] args){
        new VitaSoftJavaTasks1().task1();
    }


    public String readInput(){

        Integer num;
        String curString;

        Scanner in = new Scanner(System.in);

        System.out.print("Enter the number of words: ");

        num = in.nextInt();                                 //чтение
        num = 0;
        System.out.println("Enter words: ");

        curString = in.nextLine();
        curString = in.nextLine();

        return curString;
    }


    //Поиск самой короткой и длинной строки
    @Override
    public void task1() {

        String curString, minString = "", maxString = "";
        int curLength, minLength = 0, maxLength = 0;
        byte num;

        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of lines: ");
        num = in.nextByte();                           //чтение

        System.out.println("Enter lines: ");

        for (;num>=0;num--){

            curString = in.nextLine();                 //чтение
            curLength = curString.length();

            if (curLength >= maxLength){
                maxString = curString;
                maxLength = curLength;
            }

            if (curLength <= minLength){
                minString = curString;
                minLength = curLength;
            } else if(minLength == 0) {
                minString = curString;
                minLength = curLength;
            }
        }

        System.out.printf("MIN (%d): \"%s\"%n", minLength, minString);
        System.out.printf("MAX (%d): \"%s\"%n", maxLength, maxString);

    }

    //Упорядочивание строк по длине
    @Override
    public void task2() {

        int num;

        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of lines: ");
        num = in.nextInt();                                 //чтение

        String[] stringMass = new String[num];

        System.out.println("Enter lines: ");

        for (int i = 0; i < num ; i++ ){
            stringMass[i] = in.next();                      //чтение
        }
        Arrays.sort(stringMass, (String o1, String o2)->{

            int s1L = o1.length(),s2L = o2.length();

            if(s1L == s2L){
                return o1.compareTo(o2);
            } else if (s1L > s2L){
                return 1;
            } else {
                return -1;
            }

        });

        for (int i = 0; i < num ; i++){
            System.out.printf("(%d): \"%s\"%n", stringMass[i].length(), stringMass[i]);
        }
    }

    //Поиск строк меньше средней
    @Override
    public void task3() {

        int num,sumLength = 0, averageLength,curLength;
        Scanner in = new Scanner(System.in);

        System.out.print("Enter the number of lines: ");

        num = in.nextInt();                                 //чтение
        String[] stringMass = new String[num];

        System.out.println("Enter lines: ");


        for (int i = 0 ; i < num ; i++ ){
            stringMass[i] = in.next();                      //чтение
            sumLength += stringMass[i].length();
        }

        averageLength = (sumLength/num);

        System.out.printf("AVERAGE (%d)%n", averageLength);

        for (int i = 0 ; i < num ; i++ ){

            curLength = stringMass[i].length();

            if (curLength < averageLength){
                System.out.printf("(%d): \"%s\"%n", curLength, stringMass[i]);
            }
        }
    }

    //Поиск слова с наименьшим числом различных символов
    @Override
    public void task4() {

        int minQuantity = 0;
        int curLength;

        String curString;
        String minQuantityWord = "";
        String[] words;
        String letters;

        boolean isEq;

        curString = readInput();

        words = curString.split(" ");

        for (String word:words){
            curLength = word.length();
            letters = "";
            for(int i = 0;i<curLength;i++){
                isEq = false;

                if(letters.indexOf(word.toCharArray()[i]) != -1){
                    isEq = true;
                }

                if(!isEq){
                    letters += word.toCharArray()[i];
                }
            }

            if (minQuantity==0 || letters.length()<minQuantity){
                minQuantityWord = word;
                minQuantity = letters.length();
            }
        }

        System.out.println(minQuantityWord);

    }

    //Поиск слов с равным числом гласных и согласных
    @Override
    public void task5() {

        int num = 0;
        int numC;
        int numV;
        int curLength;

        String curString;
        String latLettersStringU = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String latLettersStringL = "abcdefghijklmnopqrstuvwxyz";

        String latLettersStringG = "AEIOUYaeiouy";

        String[] words;

        boolean err = false;

        curString = readInput();

        words = curString.split(" ");

        for (String word:words){
            curLength = word.length();

            numC = 0;
            numV = 0;

            if (curLength <= 1){
                continue;
            }

            for(int i = 0;i<curLength;i++){

                err = false;

                if(latLettersStringU.indexOf(word.toCharArray()[i]) != -1 || (latLettersStringL.indexOf(word.toCharArray()[i]) != -1)){

                    if(latLettersStringG.indexOf(word.toCharArray()[i]) != -1){
                        numV += 1;
                    }else{
                        numC += 1;
                    }
                }
                else{
                    err = true;
                    break;
                }
            }

            if (!err && numV == numC){
                num += 1;
            }
        }

        System.out.printf(String.valueOf(num));

    }

    //Поиск слова с возрастающими буквами
    @Override
    public void task6() {

        int curLength;
        String curString;

        String[] words;
        String resWord = "NOT FOUND";

        boolean err;

        curString = readInput();

        words = curString.split(" ");

        for (String word:words){
            curLength = word.length();

            if (curLength <= 1){
                continue;
            }

            err = false;

            for(int i = 1;i<curLength;i++){

                char[] CA = word.toCharArray();
                if(CA[i] < CA[i-1]){

                    err = true;
                    break;
                }
            }

            if (!err){
                resWord = word;
            }
        }

        System.out.printf(resWord);

    }

    //Поиск слова, состоящего из различных символов
    @Override
    public void task7() {

        int curLength;
        String curString;
        String resultWords = "";

        Set<String> words;
        final String errWord = "NOT FOUND";

        boolean err;
        boolean firstWord = true;

        curString = readInput();

        words = new HashSet<>();
        for(String curWord:curString.split(" ")){
            words.add(curWord);
        }

        for (String word:words){
            curLength = word.length();

            if (curLength == 0){
                continue;
            }

            err = false;

            char[] CA = word.toCharArray();

            for(int i = 0;i<curLength;i++){

                for(int j = 0;j<curLength;j++){

                    if(i != j && CA[i] == CA[j]){

                        err = true;
                        break;

                    }
                }

                if (err) {
                    break;
                }
            }

            if (!err){                      //Т.к. мы используем Set - повторений в вводе точно нет
                if(firstWord) {
                    resultWords = resultWords.concat(word);
                    firstWord = false;
                }else
                    resultWords = resultWords.concat(" ").concat(word);
            }
        }

        if (resultWords.length()>0){
            System.out.println(resultWords);
        }else{
            System.out.println(errWord);
        }

    }






















}
